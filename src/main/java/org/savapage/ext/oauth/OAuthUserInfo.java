/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.oauth;

/**
 *
 * @author Rijk Ravestein
 *
 */
public class OAuthUserInfo {

    /** The user id. */
    private String userId;
    /** Email address. */
    private String email;

    /**
     *
     * @return The user id.
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param id
     *            The user id.
     */
    public void setUserId(final String id) {
        this.userId = id;
    }

    /**
     *
     * @return Email address.
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param mailAddress
     *            Email address.
     */
    public void setEmail(final String mailAddress) {
        this.email = mailAddress;
    }

}
