/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.oauth;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.savapage.ext.ServerPlugin;

/**
 *
 * @author Rijk Ravestein
 *
 */
public interface OAuthClientPlugin extends ServerPlugin {

    /**
     * @return The OAuth provider.
     */
    OAuthProviderEnum getProvider();

    /**
     * @return ID to make plug-ins with same {@link OAuthProviderEnum} unique.
     *         If {@code null}, just one {@link OAuthProviderEnum} plug-in
     *         instance is applicable.
     */
    String getInstanceId();

    /**
     * @return Path of custom icon, relative to server/custom/web/. If
     *         {@code null}, the default stock icon is used.
     */
    String getCustomIconPath();

    /**
     * @return URL of OAuth provider where users authorize SavaPage to do OAuth
     *         calls.
     */
    URL getAuthorizationUrl();

    /**
     *
     * @return URL the OAuth provider should redirect after authorization.
     */
    URL getCallbackUrl();

    /**
     * @return If {@code true}, the OAuth provided User ID is part of SavaPage
     *         external user source.
     */
    boolean isUserSource();

    /**
     * Notifies the Web API callback.
     *
     * @param parameterMap
     *            The callback parameters and their values.
     * @return The {@link OAuthUserInfo} or {@code null} when no info is
     *         available.
     * @throws IOException
     *             When communication errors.
     * @throws OAuthPluginException
     *             When logical errors occur;
     */
    OAuthUserInfo onCallBack(Map<String, String> parameterMap)
            throws IOException, OAuthPluginException;

}
