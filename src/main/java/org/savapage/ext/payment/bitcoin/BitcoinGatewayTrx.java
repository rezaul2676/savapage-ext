/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.bitcoin;

import org.savapage.ext.payment.PaymentGatewayPlugin;

/**
 * Payment Gateway Transaction.
 *
 * @author Rijk Ravestein
 *
 */
public final class BitcoinGatewayTrx {

    private String gatewayId;
    private String transactionId;
    private String transactionAddress;
    private long satoshi;
    private String exchangeCurrencyCode;
    private double exchangeRate;
    private int confirmations;
    private String userId;

    private boolean acknowledged;
    private boolean trusted;

    /**
     *
     * @return The unique ID of the Payment Gateway. See
     *         {@link PaymentGatewayPlugin#getId()}.
     */
    public String getGatewayId() {
        return gatewayId;
    }

    /**
     * Sets the unique ID of the Payment Gateway. See
     * {@link PaymentGatewayPlugin#getId()}.
     *
     * @param gatewayId
     *            The ID.
     */
    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionAddress() {
        return transactionAddress;
    }

    public void setTransactionAddress(String transactionAddress) {
        this.transactionAddress = transactionAddress;
    }

    public int getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(int confirmations) {
        this.confirmations = confirmations;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getSatoshi() {
        return satoshi;
    }

    public void setSatoshi(long satoshi) {
        this.satoshi = satoshi;
    }

    public String getExchangeCurrencyCode() {
        return exchangeCurrencyCode;
    }

    public void setExchangeCurrencyCode(String exchangeCurrencyCode) {
        this.exchangeCurrencyCode = exchangeCurrencyCode;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public boolean isTrusted() {
        return trusted;
    }

    public void setTrusted(boolean trusted) {
        this.trusted = trusted;
    }

}
